package com.epam.stakhiv;

import com.epam.stakhiv.models.Message;
import com.epam.stakhiv.view.ConsoleView;
import com.epam.stakhiv.view.Menu;

public class Start {

    public static void main(String[] args) {
        new Menu().mainMenu();
        new Message(new ConsoleView());
    }
}
