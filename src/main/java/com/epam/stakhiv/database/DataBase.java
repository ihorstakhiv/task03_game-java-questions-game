package com.epam.stakhiv.database;

import com.epam.stakhiv.models.Answer;
import com.epam.stakhiv.models.Question;
import com.epam.stakhiv.models.TypeOfQuestion;

import java.util.List;
import java.util.stream.Collectors;

public class DataBase {

    List<Question> allQuestions;
    List<Answer> allAnswers;

    public List<Question> fillUpAllQuestions() {
        allQuestions.add(new Question("Which of the following Java operators can be used with boolean variables? ", TypeOfQuestion.Operators_and_Statements));
        allQuestions.add(new Question("What data type (or types) will allow the following code snippet to compile? " + "\n" + "byte x = 5;" + "\n" + "byte y = 10;" + "\n" + "___z = x + y;", TypeOfQuestion.Operators_and_Statements));
        allQuestions.add(new Question("What is the result of the following code snippet?" + "\n" + " 3: int m = 9, n = 1, x = 0;" + "\n" + " 4: while(m > n) {" + "\n" + " 5:   m--;" + "\n" + " 6:   n += 2; " + "\n" + " 7:   x += m + n;" + "\n" + " 8: }" + "\n" + " 9: System.out.println(x);", TypeOfQuestion.Operators_and_Statements));
        allQuestions.add(new Question("What is the output of the following code snippet?" + "\n" + " 3: do {" + "\n" + " 4:   int y = 1;" + "\n" + " 5:   System.out.print(y++);" + "\n" + " 6: } while(y <= 10);", TypeOfQuestion.Operators_and_Statements));
        allQuestions.add(new Question(" What is the output of the following code snippet?" + "\n" + "3: int x1 = 50, x2 = 75;" + "\n" + "4: boolean b = x1 >= x2;" + "\n" + "5: if(b = true) System.out.println(\"Success\");" + "\n" + "6: else System.out.println(\"Failure\"); ", TypeOfQuestion.Operators_and_Statements));

        allQuestions.add(new Question("Which of the following are valid Java identifiers? ", TypeOfQuestion.Building_Blocks));
        allQuestions.add(new Question("Which of the following are legal entry point methods that can be run from the command line?", TypeOfQuestion.Building_Blocks));
        allQuestions.add(new Question("Which of the following are true?", TypeOfQuestion.Building_Blocks));
        allQuestions.add(new Question(" Which of the following are true? ", TypeOfQuestion.Building_Blocks));
        allQuestions.add(new Question("Which of the following are true? ", TypeOfQuestion.Building_Blocks));

        allQuestions.add( new Question("How to integrate current state with origin master? ", TypeOfQuestion.Git));
        allQuestions.add(new Question("Which sub-directory of Git contains all necessary repository files?", TypeOfQuestion.Git));
        allQuestions.add(new Question("What type of version control software does Git is?", TypeOfQuestion.Git));
        allQuestions.add(new Question(" How do you see your Git repository's commit history? ", TypeOfQuestion.Git));
        allQuestions.add(new Question(" How can you abort an in-progress rebase? ", TypeOfQuestion.Git));
        return allQuestions;
    }

    public List<Answer> fillUpAllAnswers() {
        allAnswers.add(new Answer("1. ==", "2. +", "3. --", "4. %", 1, TypeOfQuestion.Operators_and_Statements));
        allAnswers.add(new Answer("1. short", "2. boolean", "3. int", "4. byte", 3, TypeOfQuestion.Operators_and_Statements));
        allAnswers.add(new Answer("1. 11", "2. 13", "3. 23", "4. 36", 4, TypeOfQuestion.Operators_and_Statements));
        allAnswers.add(new Answer("1. 1 2 3 4 5 6 7 8 9 10", "2. 1 2 3 4 5 6 7 8 9", "3. The code will not compile because of line 6.", "4. The code contains an infinite loop and does not terminate.", 4, TypeOfQuestion.Operators_and_Statements));
        allAnswers.add(new Answer("1. Success", "2. Failure ", "3. The code will not compile because of line 4.", "4. The code will not compile because of line 5", 3, TypeOfQuestion.Operators_and_Statements));

        allAnswers.add(new Answer("1. true", "2. java.lang", "3. Public ", "4. 1980_s", 3, TypeOfQuestion.Building_Blocks));
        allAnswers.add(new Answer("1. public static void main(String[] args) ", "2. public static final main(String[] args)", "3. public static main(String[] args) ", "4. public void main(String[] args) ", 1, TypeOfQuestion.Building_Blocks));
        allAnswers.add(new Answer("1.An instance variable of type double defaults to null", "2. An instance variable of type String defaults to nul", "3. An instance variable of type String defaults to 0.0", "4. An instance variable of type int defaults to null.", 2, TypeOfQuestion.Building_Blocks));
        allAnswers.add(new Answer("1. None of the above", "2. A local variable of type float defaults to 0.", "3. A local variable of type boolean defaults to false", "4. A local variable of type Object defaults to null", 1, TypeOfQuestion.Building_Blocks));
        allAnswers.add(new Answer("1. package my.directory.named.a", "2. package my.directory.named.A", "3. package named.a", "4. package a", 1, TypeOfQuestion.Building_Blocks));

        allAnswers.add(new Answer("1. Git init command on a empty directory", "2. Git init command in a directory containing project files already", "3. Clone an existing project from somewhere else (e.g. GitHub) ", "4. All of the above", 4, TypeOfQuestion.Git));
        allAnswers.add(new Answer("1. .git ", "2. .conf", "3. .config ", "4. None of the above ", 1, TypeOfQuestion.Git));
        allAnswers.add(new Answer("1. None of the above", "2. Distributed VCS", "3. Local VCS", "4. Centralized CVS", 2, TypeOfQuestion.Git));
        allAnswers.add(new Answer("1. commit", "2. history", "3. add", "4. log", 4, TypeOfQuestion.Git));
        allAnswers.add(new Answer("1. git reset", "2. git commit --amend", "3. git rebase --abort", "4. git clean", 3, TypeOfQuestion.Git));
        return allAnswers;
    }

    public List<Question> fillUpSomeCategoryQuestionsOAS() {
        List<Question> listOperatorsAndStatements = fillUpAllQuestions().stream().filter(question -> question.typeOfQuestion.equals(TypeOfQuestion.Operators_and_Statements)).collect(Collectors.toList());
        return  listOperatorsAndStatements;
    }

    public List<Answer> fillUpSomeCategoryAnswersOAS() {
        List<Answer> listAnswersOperatorsAndStatements = fillUpAllAnswers().stream().filter(answer -> answer.typeOfQuestion.equals(TypeOfQuestion.Operators_and_Statements)).collect(Collectors.toList());
        return  listAnswersOperatorsAndStatements;
    }

    public List<Question> fillUpSomeCategoryQuestionsBB() {
        List<Question> listBuildingBlocks = fillUpAllQuestions().stream().filter(question -> question.typeOfQuestion.equals(TypeOfQuestion.Building_Blocks)).collect(Collectors.toList());
        return listBuildingBlocks;
    }

    public List<Answer> fillUpSomeCategoryAnswersBB() {
        List<Answer> listAnswersBuildingBlocks = fillUpAllAnswers().stream().filter(answer -> answer.typeOfQuestion.equals(TypeOfQuestion.Building_Blocks)).collect(Collectors.toList());
        return  listAnswersBuildingBlocks;
    }

    public List<Question> fillUpSomeCategoryQuestionsGit() {
        List<Question> listGit = fillUpAllQuestions().stream().filter(question -> question.typeOfQuestion.equals(TypeOfQuestion.Git)).collect(Collectors.toList());
        return  listGit;
    }

    public List<Answer> fillUpSomeCategoryAnswersGit() {
        List<Answer> listAnswersGit = fillUpAllAnswers().stream().filter(answer -> answer.typeOfQuestion.equals(TypeOfQuestion.Git)).collect(Collectors.toList());
        return  listAnswersGit;
    }
}
