package com.epam.stakhiv.models;

import com.epam.stakhiv.view.AbstractView;

final public class Message {

    AbstractView abstractView;

    static final String mainMenu = "                     Menu" + "\n" + "             ____________________";
    static final String singlePlayer = "            < 1.  Single game   >";
    static final String doublePlayers = "            < 2. Multiplay game >";
    static final String rules = "            < 3.     Rules      >";
    static final String about = "            < 4.     About      >";
    static final String choose = "Write your choice: ";
    static final String space = " ";
    static final String previousMenu = "    previous page ";
    static final String oas = " - 1)            Operators and statements                      -";
    static final String bb = " - 2)                 Building blocks                          -";
    static final String git = " - 3)                      Git                                 -";
    static final String rule = "- Select the desired mode\n" +
            "- You need to indicate the correct answers to the questions.\n" +
            "- The result will be displayed at the end of the test.";
    static final String aboutTxt = "\n" +
            "This game is for one or two players.\n" +
            "In each of the modes you can see many. " +
            "sub-modes of the game with your own settings.\n" +
            "You can compete with your friends.\n" +
            "After finishing, you can see all the errors and find out what topic to repeat.\n" +
            " \n" +
            "This game was created for those who want to improve java."+
            "\n" +
            "* created by Ihor Stakhiv *";

    public Message(AbstractView abstractView) {
        this.abstractView = abstractView;
    }

    public void djksbhdajh()
    {
        abstractView.print("dsadsd");
    }

    public static void printAllMenu() {

        final String firstPosition = " - 1)                   All questions                          -";
        final String secondPosition = " - 2)           Questions from categories (Choose some)        -";

        System.out.print(space + space + space);

        System.out.println(firstPosition);
        System.out.println(secondPosition);
        System.out.println(space);
        System.out.println(previousMenu);
        System.out.println(space);
        System.out.print(choose);
    }

    public static void printAllMenuDouble() {

        final String firstPosition = " - 1)          All questions                  -";

        System.out.println(firstPosition);
        System.out.println(space);
        System.out.println(previousMenu);
        System.out.println(space);
        System.out.print(choose);
    }

    public static void printAllMenuSecond() {

        final String secondMenuFirstPosition = " - 1)            Fixed time(you must set time)                 -";
        final String secondMenuSecondPosition = " - 2)            Fixed life(you have 5 life)                   -";
        final String secondMenuThirdPosition = " - 3)     Fixed times of game(you must set times of questions) -";

        System.out.println(space);
        System.out.println(secondMenuFirstPosition);
        System.out.println(secondMenuSecondPosition);
        System.out.println(secondMenuThirdPosition);
        System.out.println(space);
        System.out.println(previousMenu);
        System.out.println(space);
        System.out.print(choose);
    }

    public static void printResultMenu() {

        final String firstPosition = " - 1)                      True answers                       -";
        final String secondPosition = " - 2)                      False answers                      -";
        final String thirdPosition = " - 3)                      Details and themes to repeat        -";
        final String fourthPosition = "     Main menu                  ";

        System.out.println(space);
        System.out.println(firstPosition);
        System.out.println(secondPosition);
        System.out.println(thirdPosition);
        System.out.println(space);
        System.out.println(fourthPosition);
        System.out.println(space);
        System.out.print(choose);
    }


    public static String getMainMenu() {
        return mainMenu;
    }

    public static String getSinglePlayer() {
        return singlePlayer;
    }

    public static String getDoublePlayers() {
        return doublePlayers;
    }

    public static String getRules() {
        return rules;
    }

    public static String getAbout() {
        return about;
    }

    public static String getChoose() {
        return choose;
    }

    public static String getSpace() {
        return space;
    }

    public static String getPreviousMenu() {
        return previousMenu;
    }

    public static String getOas() {
        return oas;
    }

    public static String getBb() {
        return bb;
    }

    public static String getGit() {
        return git;
    }

    public static String getRule() {
        return rule;
    }

    public static String getAboutTxt() {
        return aboutTxt;
    }


}
