package com.epam.stakhiv.models;

public enum TypeOfQuestion {

    Operators_and_Statements,
    Building_Blocks,
    Git
}
