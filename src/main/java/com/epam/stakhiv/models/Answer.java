package com.epam.stakhiv.models;

import java.util.Objects;

public class Answer {

    private String variableA;
    private String variableB;
    private String variableC;
    private String variableD;
    private int trueAnswer;
    public TypeOfQuestion typeOfQuestion;

     public Answer(String variableA, String variableB, String variableC, String variableD, int trueAnswer, TypeOfQuestion typeOfQuestion) {
        this.variableA = variableA;
        this.variableB = variableB;
        this.variableC = variableC;
        this.variableD = variableD;
        this.trueAnswer = trueAnswer;
        this.typeOfQuestion = typeOfQuestion;
    }

    public String getVariableA() {
        return variableA;
    }

    public String getVariableB() {
        return variableB;
    }

    public String getVariableC() {
        return variableC;
    }

    public String getVariableD() {
        return variableD;
    }

    public int getTrueAnswer() {
        return trueAnswer;
    }

    public TypeOfQuestion getTypeOfQuestion() {
        return typeOfQuestion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return trueAnswer == answer.trueAnswer &&
                Objects.equals(variableA, answer.variableA) &&
                Objects.equals(variableB, answer.variableB) &&
                Objects.equals(variableC, answer.variableC) &&
                Objects.equals(variableD, answer.variableD) &&
                typeOfQuestion == answer.typeOfQuestion;
    }

    @Override
    public int hashCode() {
        return Objects.hash(variableA, variableB, variableC, variableD, trueAnswer, typeOfQuestion);
    }

    @Override
    public String toString() {
        return "Answer{" +
                "variableA='" + variableA + '\'' +
                ", variableB='" + variableB + '\'' +
                ", variableC='" + variableC + '\'' +
                ", variableD='" + variableD + '\'' +
                ", trueAnswer=" + trueAnswer +
                ", typeOfQuestion=" + typeOfQuestion +
                '}';
    }
}
