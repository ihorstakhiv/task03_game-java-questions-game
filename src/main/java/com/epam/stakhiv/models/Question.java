package com.epam.stakhiv.models;

import java.util.Objects;

public class Question {

    private String question;
    private int coin = 1;
    public TypeOfQuestion typeOfQuestion;

    public Question(String question, TypeOfQuestion typeOfQuestion) {
        this.question = question;
        this.typeOfQuestion = typeOfQuestion;
    }

    public Question() {
    }

    public String getQuestion() {
        return question;
    }

    public TypeOfQuestion getTypeOfQuestion() {
        return typeOfQuestion;
    }

    public void setTypeOfQuestion(TypeOfQuestion typeOfQuestion) {
        this.typeOfQuestion = typeOfQuestion;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question1 = (Question) o;
        return coin == question1.coin &&
                Objects.equals(question, question1.question) &&
                typeOfQuestion == question1.typeOfQuestion;
    }

    @Override
    public int hashCode() {
        return Objects.hash(question, typeOfQuestion, coin);
    }

    @Override
    public String toString() {
        return "Question{" +
                "question='" + question + '\'' +
                ", typeOfQuestion=" + typeOfQuestion +
                ", coin=" + coin +
                '}';
    }
}
