package com.epam.stakhiv.models;

import java.util.ArrayList;
import java.util.Objects;

public class Player {

    private String name;
    private int point = 0;
    private int life = 5;
    ArrayList<Question> trueQuestions = new ArrayList<>();
    ArrayList<Question> falseQuestions = new ArrayList<>();

    public Player() {


    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public ArrayList<Question> getTrueQuestions() {
        return trueQuestions;
    }

    public ArrayList<Question> getFalseQuestions() {
        return falseQuestions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return point == player.point &&
                life == player.life &&
                Objects.equals(name, player.name) &&
                Objects.equals(trueQuestions, player.trueQuestions) &&
                Objects.equals(falseQuestions, player.falseQuestions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, point, life, trueQuestions, falseQuestions);
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", point=" + point +
                ", life=" + life +
                ", trueQuestions=" + trueQuestions +
                ", falseQuestions=" + falseQuestions +
                '}';
    }
}
