package com.epam.stakhiv.exception;

import java.util.InputMismatchException;

public class InvalidNumberException extends InputMismatchException {

    public static String GET_TWO_VARIABLE_EXCEPTION() {
        return "Please enter number from 1 to 2       !!!";
    }

    public static String GET_THREE_VARIABLES_EXCEPTION() {
        return "Please enter number from 1 to 3       !!!";
    }

    public static String GET_FOUR_VARIABLES_EXCEPTION() {
        return "Please enter number from 1 to 4       !!!";
    }

}
