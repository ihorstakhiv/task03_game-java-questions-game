package com.epam.stakhiv.view;

import com.epam.stakhiv.controllers.GameLogicController;
import com.epam.stakhiv.exception.InvalidNumberException;
import com.epam.stakhiv.help.Const;
import com.epam.stakhiv.database.DataBase;
import com.epam.stakhiv.models.Message;
import java.util.Scanner;

public class Menu {

    Scanner scanner = new Scanner(System.in);
    GameLogicController gameLogic;
    DataBase dataBase;
    DoublePlayersGameMenu doublePlayersGameMenu;

    public void mainMenu() {
        System.out.println(Message.getSpace());
        System.out.println(Message.getSpace());
        System.out.println(Message.getMainMenu());
        System.out.println(Message.getSpace());
        System.out.println(Message.getSinglePlayer());
        System.out.println(Message.getDoublePlayers());
        System.out.println(Message.getRules());
        System.out.println(Message.getAbout());
        System.out.println(Message.getSpace());
        System.out.print(Message.getChoose());
        mainMenuChoice();
    }

    public void mainMenuChoice() {
        int number = scanner.nextInt();
        exceptionChecker(number, Const.CYCLE_FOUR, InvalidNumberException.GET_FOUR_VARIABLES_EXCEPTION());
        switch (number) {
            case 1:
                System.out.println(Message.getSpace());
                System.out.println(Message.getSinglePlayer());
                System.out.println(Message.getSpace());
                singleGameMenu();
                break;
            case 2:
                System.out.println(Message.getSpace());
                System.out.println(Message.getDoublePlayers());
                System.out.println(Message.getSpace());
                doublePlayersGameMenu.multiplayGameMenu();
                break;
            case 3:
                System.out.println(Message.getSpace());
                System.out.println(Message.getRules());
                System.out.println(Message.getSpace());
                getRules();
                break;
            case 4:
                System.out.println(Message.getSpace());
                System.out.println(Message.getAbout());
                System.out.println(Message.getSpace());
                getAbout();
                break;
        }
    }

    public void singleGameMenu() {
        Message.printAllMenu();
        int number = scanner.nextInt();
        exceptionChecker(number, Const.CYCLE_THREE, InvalidNumberException.GET_THREE_VARIABLES_EXCEPTION());
        switch (number) {
            case 1:
                Message.printAllMenuSecond();
                singleGameMenuFirst();
                break;
            case 2:
                Message.printAllMenuSecond();
                singleGameMenuSecond();
                break;
            case 3:
                mainMenu();
                break;
        }
    }

    public void singleGameMenuFirst() {
        int number = scanner.nextInt();
        exceptionChecker(number, Const.CYCLE_FOUR, InvalidNumberException.GET_FOUR_VARIABLES_EXCEPTION());
        switch (number) {
            case 1:
                System.out.println(Message.getSpace());
                String playerName0 = setPlayerName();
                System.out.println("Set time (in seconds)");
                int time = scanner.nextInt();
                gameLogic.fixedTimeMenu(playerName0, time);
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 2:
                String playerName1 = setPlayerName();
                gameLogic.fixedHealthGame(playerName1);
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 3:
                String playerName2 = setPlayerName();
                System.out.println("Enter How many tests you want :");
                int count = scanner.nextInt();
                gameLogic.fixedTimes(playerName2, count);
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 4:
                singleGameMenu();
                break;
        }
    }

    public void singleGameMenuSecond() {
        int number = scanner.nextInt();
        exceptionChecker(number, Const.CYCLE_FOUR, InvalidNumberException.GET_FOUR_VARIABLES_EXCEPTION());
        switch (number) {
            case 1:
                singleGameMenuThirdOne();
                break;
            case 2:
                singleGameMenuThirdTwo();
                break;
            case 3:
                singleGameMenuThirdThree();
                break;
            case 4:
                singleGameMenuSecond();
                break;
        }
    }

    public void singleGameMenuThirdOne() {
        System.out.println(Message.getSpace());
        System.out.println(Message.getOas());
        System.out.println(Message.getBb());
        System.out.println(Message.getGit());
        System.out.println(Message.getSpace());
        System.out.println(Message.getPreviousMenu());
        System.out.println(Message.getSpace());
        System.out.print(Message.getChoose());
        int number = scanner.nextInt();
        exceptionChecker(number, Const.CYCLE_FOUR, InvalidNumberException.GET_FOUR_VARIABLES_EXCEPTION());
        switch (number) {
            case 1:
                System.out.println("Set time (in seconds): ");
                int time1 = scanner.nextInt();
                gameLogic.fixedTimeMenuCategory(setPlayerName(), time1, dataBase.fillUpSomeCategoryQuestionsOAS(), dataBase.fillUpSomeCategoryAnswersOAS());
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 2:
                System.out.println("Set time (in seconds): ");
                int time2 = scanner.nextInt();
                gameLogic.fixedTimeMenuCategory(setPlayerName(), time2, dataBase.fillUpSomeCategoryQuestionsBB(), dataBase.fillUpSomeCategoryAnswersBB());
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 3:
                System.out.println("Set time (in seconds): ");
                int time3 = scanner.nextInt();
                gameLogic.fixedTimeMenuCategory(setPlayerName(), time3, dataBase.fillUpSomeCategoryQuestionsGit(), dataBase.fillUpSomeCategoryAnswersGit());
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 4:
                singleGameMenu();
                break;
        }
    }

    public void singleGameMenuThirdTwo() {
        System.out.println(Message.getOas());
        System.out.println(Message.getBb());
        System.out.println(Message.getGit());
        System.out.println(Message.getPreviousMenu());
        System.out.print(Message.getChoose());
        int number = scanner.nextInt();
        exceptionChecker(number, Const.four, InvalidNumberException.getMessage4());
        switch (number) {
            case 1:
                gameLogic.fixedHealthGameCategory(setPlayerName(), dataBase.fillUpSomeCategoryQuestionsOAS(), dataBase.fillUpSomeCategoryAnswersOAS());
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 2:
                gameLogic.fixedHealthGameCategory(setPlayerName(), dataBase.fillUpSomeCategoryQuestionsBB(), dataBase.fillUpSomeCategoryAnswersBB());
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 3:
                gameLogic.fixedHealthGameCategory(setPlayerName(), dataBase.fillUpSomeCategoryQuestionsGit(), dataBase.fillUpSomeCategoryAnswersGit());
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 4:
                singleGameMenu();
                break;
        }
    }

    public void singleGameMenuThirdThree() {
        System.out.println(Message.getOas());
        System.out.println(Message.getBb());
        System.out.println(Message.getGit());
        System.out.println(Message.getPreviousMenu());
        System.out.print(Message.getChoose());

        int number = scanner.nextInt();
        exceptionChecker(number, Const.four, InvalidNumberException.getMessage4());
        switch (number) {
            case 1:
                System.out.println("Enter How many tests you want :");
                int count1 = scanner.nextInt();
                gameLogic.fixedTimesCategory(setPlayerName(), count1, dataBase.fillUpSomeCategoryQuestionsOAS(), dataBase.fillUpSomeCategoryAnswersOAS());
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 2:
                System.out.println("Enter How many tests you want :");
                int count2 = scanner.nextInt();
                gameLogic.fixedTimesCategory(setPlayerName(), count2, dataBase.fillUpSomeCategoryQuestionsBB(), dataBase.fillUpSomeCategoryAnswersBB());
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 3:
                System.out.println("Enter How many tests you want :");
                int count3 = scanner.nextInt();
                gameLogic.fixedTimesCategory(setPlayerName(), count3, dataBase.fillUpSomeCategoryQuestionsGit(), dataBase.fillUpSomeCategoryAnswersGit());
                gameLogic.printPlayer(gameLogic.getPlayer1());
                break;
            case 4:
                singleGameMenuThirdThree();
                break;
        }
    }

    public String setPlayerName() {
        System.out.println(Message.getSpace());
        System.out.print("Enter your name :");
        String name = scanner.next();
        return name;
    }

    public void getRules() {
        System.out.println(Message.getRule());
        System.out.println(Message.getSpace() + Message.getSpace());
        System.out.println(Message.getSinglePlayer());
        System.out.println(Message.getDoublePlayers());
        System.out.println(Message.getPreviousMenu());
        System.out.print(Message.getChoose());
        int number = scanner.nextInt();
        exceptionChecker(number, Const.four, InvalidNumberException.getMessage4());
        getRulesAndGetAboutMenu(number);
    }

    public void getAbout() {
        System.out.println(Message.getAboutTxt());
        System.out.println(Message.getSpace() + Message.getSpace());
        System.out.println(Message.getSinglePlayer());
        System.out.println(Message.getDoublePlayers());
        System.out.println(Message.getPreviousMenu());
        System.out.print(Message.getChoose());
        int number = scanner.nextInt();
        getRulesAndGetAboutMenu(number);
    }

    public void exceptionChecker(int number, int limit, String message) {
        try {
            if (number <= Const.ZERO || number > limit)
                throw new InvalidNumberException();
        } catch (InvalidNumberException exception) {
            System.err.println(message);
            mainMenu();
        }
    }

    public void getRulesAndGetAboutMenu(int number) {
        switch (number) {
            case 1:
                singleGameMenu();
                break;
            case 2:
                doublePlayersGameMenu.multiplayGameMenu();
                break;
            case 3:
                mainMenu();
                break;
        }
    }
}
