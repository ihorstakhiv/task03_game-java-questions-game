package com.epam.stakhiv.view;

abstract public class AbstractView {

    abstract public void print(String s);
}
