package com.epam.stakhiv.view;

import com.epam.stakhiv.controllers.GameLogicController;
import com.epam.stakhiv.exception.InvalidNumberException;
import com.epam.stakhiv.help.Const;
import com.epam.stakhiv.models.Message;
import java.util.Scanner;

class DoublePlayersGameMenu {

    public void multiplayGameMenu() {

        Scanner scanner = new Scanner(System.in);
        Menu menu = new Menu();
        GameLogicController gameLogicController = new GameLogicController();
        Message.printAllMenuDouble();
        int number = scanner.nextInt();
        menu.exceptionChecker(number, Const.two, InvalidNumberException.getMessage2());
         switch (number) {
            case 1:
                Message.printAllMenuSecond();
                int number2 = scanner.nextInt();
                menu.exceptionChecker(number, Const.four, InvalidNumberException.getMessage4());
                switch (number2) {
                    case 1:
                        System.out.println("\n" + "First player, please set name !");
                        String name1 = menu.setPlayerName();
                        System.out.println("\n" + "Second player, please set name !");
                        String name2 = menu.setPlayerName();
                        System.out.println("\n" + "Set time (in seconds) : ");
                        int second = scanner.nextInt();
                        System.out.println("\n" + "First player  lets-go!");
                        gameLogicController.fixedTimeMenu(name1, second);
                        System.out.println("\n" + "Second player  lets-go!");
                        gameLogicController.fixedTimeMenu2(name2, second);
                        gameLogicController.printTwoPlayers();
                        break;
                    case 2:
                        System.out.println("\n" + "First player, please set name !");
                        String name3 = menu.setPlayerName();
                        System.out.println("\n" + "Second player, please set name !");
                        String name4 = menu.setPlayerName();
                        System.out.println("\n" + "First player  lets-go!");
                        gameLogicController.fixedHealthGame(name3);
                        System.out.println("\n" + "Second player  lets-go!");
                        gameLogicController.fixedHealthGame2(name4);
                        gameLogicController.printTwoPlayers();
                        break;
                    case 3:
                        System.out.println("\n" + "First player, please set name !");
                        String name5 = menu.setPlayerName();
                        System.out.println("\n" + "Second player, please set name !");
                        String name6 = menu.setPlayerName();
                        System.out.println("\n" + "Enter How many tests you want :");
                        int count = scanner.nextInt();
                        System.out.println("\n" + "First player  lets-go!");
                        gameLogicController.fixedTimes(name5, count);
                        System.out.println("\n" + "Second player  lets-go!");
                        gameLogicController.fixedTimes2(name6, count);
                        gameLogicController.printTwoPlayers();
                        break;
                    case 4:
                        menu.doublePlayersGameMenu.multiplayGameMenu();
                        break;
                }
                break;
            case 2:
                menu.mainMenu();
                break;
        }
    }
}

