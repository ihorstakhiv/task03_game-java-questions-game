package com.epam.stakhiv.help;

public class Const {

    public  static int START_CYCLE = 0;
    public  static int CYCLE_ONE = 1;
    public  static int CYCLE_TWO = 2;
    public  static int CYCLE_THREE = 3;
    public  static int CYCLE_FOUR = 4;
    public  static int CALCULATE_PERCENT_HUNDRED = 100;

    private Const() {
    }

}
