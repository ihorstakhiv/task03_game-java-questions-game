package com.epam.stakhiv.controllers;

import com.epam.stakhiv.database.DataBase;
import com.epam.stakhiv.exception.InvalidNumberException;
import com.epam.stakhiv.help.Const;
import com.epam.stakhiv.models.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class GameLogicController {

    private DataBase dataBase;
    Scanner sc = new Scanner(System.in);
    private Player player1 = new Player();
    private Player player2 = new Player();

    public void fixedTimeMenu(String playerName, int time) {
        player1.setName(playerName);
        for (long stop = System.nanoTime() + TimeUnit.SECONDS.toNanos(time); stop > System.nanoTime(); ) {
            getAndCheckQuestion(player1, dataBase.fillUpAllQuestions(), dataBase.fillUpAllAnswers());
        }
    }

    public Player fixedTimes(String playerName, int times) {
        player1.setName(playerName);
        IntStream.range(Const.ZERO, times).forEach(i -> getAndCheckQuestion(player1, dataBase.fillUpAllQuestions(), dataBase.fillUpAllAnswers()));
        return player1;
    }

    public Player fixedTimesCategory(String playerName, int times, ArrayList<Question> questions, ArrayList<Answer> answers) {
        player1.setName(playerName);
        IntStream.range(Const.ZERO, times).forEach(k -> getAndCheckQuestion(player1, questions, answers));
        return player1;
    }

    public Player fixedHealthGame(String playerName) {
        player1.setName(playerName);
        while (player1.getLife() > Const.ZERO) {
            getAndCheckQuestion(player1, dataBase.fillUpAllQuestions(), dataBase.fillUpAllAnswers());
        }
        return player1;
    }

    public Player fixedTimeMenuCategory(String playerName, int time, ArrayList<Question> questions, ArrayList<Answer> answers) {
        player1.setName(playerName);
        for (long stop = System.nanoTime() + TimeUnit.SECONDS.toNanos(time); stop > System.nanoTime(); ) {
            getAndCheckQuestion(player1, questions, answers);
        }
        return player1;
    }

    public Player fixedHealthGameCategory(String playerName, ArrayList<Question> questions, ArrayList<Answer> answers) {
        player1.setName(playerName);
        for (int j = player1.getLife(); player1.getLife() > Const.ZERO; j--) {
            getAndCheckQuestion(player1, questions, answers);
        }
        return player1;
    }



    public Player fixedTimeMenu2(String playerName, int time) {
        player2.setName(playerName);
        for (long stop = System.nanoTime() + TimeUnit.SECONDS.toNanos(time); stop > System.nanoTime(); ) {
            getAndCheckQuestion(player2, dataBase.fillUpAllQuestions(), dataBase.fillUpAllAnswers());
        }
        return player2;
    }

    public Player fixedHealthGame2(String playerName) {
        player2.setName(playerName);
        for (int j = player2.getLife(); player2.getLife() > Const.ZERO; j--) {
            getAndCheckQuestion(player2, dataBase.fillUpAllQuestions(), dataBase.fillUpAllAnswers());
        }
        return player2;
    }

    public Player fixedTimes2(String playerName, int times) {
        player2.setName(playerName);
        for (int k = Const.ZERO; k < times; k++) {
            getAndCheckQuestion(player2, dataBase.fillUpAllQuestions(), dataBase.fillUpAllAnswers());
        }
        return player2;
    }

    private int getRandomNumbers(Collection<Question> questionsOfSomeCategory) {
        double r = Math.random() * questionsOfSomeCategory.size();
        return (int) r;
    }

    public void printPlayer(Player player) {
        System.out.println("Player : " + player.getName());
        System.out.println("Your points :" + player.getPoint());
        Message.printResultMenu();
        getResult();
    }

    public void printTwoPlayers() {
        System.out.println("Player name: " + player1.getName());
        System.out.println("your points :" + player1.getPoint());
        System.out.println("Player name: " + player2.getName());
        System.out.println("your points :" + player2.getPoint());
        if (player1.getPoint() > player2.getPoint()) {
            System.out.println("\n" + "Winner is  <<<<<<<<<<<________________" + player1.getName() + "______________>>>>>>>>>");
        } else if (player1.getPoint() == player2.getPoint()) {
            System.out.println("\n" + "<<<<<<<<<<<_________________Draw_______________>>>>>>>>>");
        } else {
            System.out.println("\n" + "Winner is  <<<<<<<<<<<________________" + player2.getName() + "______________>>>>>>>>>");
        }
    }

    private void getAndCheckQuestion(Player player, ArrayList<Question> questions, ArrayList<Answer> answers) {
        int randomNumber = getRandomNumbers(questions);
        System.out.println(Message.getSpace());
        System.out.println(questions.get(randomNumber).getQuestion());
        System.out.println(Message.getSpace());
        System.out.println(answers.get(randomNumber).getVariableA());
        System.out.println(answers.get(randomNumber).getVariableB());
        System.out.println(answers.get(randomNumber).getVariableC());
        System.out.println(answers.get(randomNumber).getVariableD());
        System.out.println(Message.getSpace());
        System.out.println(Message.getChoose());

        int answer = sc.nextInt();
        try {
            if (answer <= Const.ZERO || answer > Const.four)
                throw new InvalidNumberException();
        } catch (InvalidNumberException exception) {
            System.err.println(InvalidNumberException.getMessage4());
            System.err.println("be more attentive!");
        }
        if (answer == answers.get(randomNumber).getTrueAnswer()) {
            player.getTrueQuestions().add(questions.get(randomNumber));
            player.setPoint(player.getPoint() + Const.one);
        } else {
            player.getFalseQuestions().add(questions.get(randomNumber));
            player.setLife(player.getLife() - Const.one);
        }
    }

    private void getResult() {
        int number = sc.nextInt();
        switch (number) {
            case 1:
                generateTrueAndFalseQuestions(player1.getTrueQuestions(), "True questions :");
                break;
            case 2:
                generateTrueAndFalseQuestions(player1.getFalseQuestions(), "False questions");
                break;
            case 3:
                getResultDetailsOfTest();
                break;
            case 4:
                printPlayer(player1);
                break;
            default:
                System.err.println("Please enter number from 1 to 4       !!!");
                getResult();
                break;
        }
    }

    private void generateTrueAndFalseQuestions(ArrayList<Question> questions, String message) {
        Set<Question> questionsSet = new HashSet<>(questions);
        System.out.println("_____________________" + message + "______________________");
        for (Question question : questionsSet) {
            System.out.println(Message.getSpace());
            System.out.println(question.getQuestion());
            System.out.println(Message.getSpace());
        }
        System.out.println("1 " + Message.getPreviousMenu());
        System.out.println();

        switch (sc.nextInt()) {
            case 1:
                printPlayer(player1);
                break;
        }
    }

    private void getResultDetailsOfTest() {
        int countOfTests = player1.getTrueQuestions().size() + player1.getFalseQuestions().size();
        double progress = (player1.getPoint() * Const.hundred) / countOfTests;
        System.out.println("Your progress : " + progress + " %");
        ArrayList<Question> falseQuestions = player1.getFalseQuestions();
        long countOAS = falseQuestions.stream().filter(question -> question.getTypeOfQuestion().equals(TypeOfQuestion.Operators_and_Statements)).count();
        long countBB = falseQuestions.stream().filter(question -> question.getTypeOfQuestion().equals(TypeOfQuestion.Building_Blocks)).count();
        long countGit = falseQuestions.stream().filter(question -> question.getTypeOfQuestion().equals(TypeOfQuestion.Git)).count();
        if (countOAS > countBB && countOAS > countGit) {
            System.err.println("Repeat : Operators and Statements topic ");
        } else if (countBB > countOAS && countBB > countGit) {
            System.err.println("Repeat : Building Blocks topic ");
        } else if (countGit > countOAS && countGit > countBB) {
            System.err.println("Repeat : Git topic ");
        } else {
            System.err.println("Your result is great ");
        }
    }

    public GameLogicController() {
    }

    public Player getPlayer1() {
        return player1;
    }
}

